package io.github.quickmsg.core.protocol;

import io.github.quickmsg.common.acl.AclAction;
import io.github.quickmsg.common.acl.AclManager;
import io.github.quickmsg.common.channel.MqttChannel;
import io.github.quickmsg.common.context.ReceiveContext;
import io.github.quickmsg.common.enums.ChannelStatus;
import io.github.quickmsg.common.message.*;
import io.github.quickmsg.common.metric.CounterType;
import io.github.quickmsg.common.metric.MetricManagerHolder;
import io.github.quickmsg.common.protocol.Protocol;
import io.github.quickmsg.common.topic.SubscribeTopic;
import io.github.quickmsg.common.topic.TopicRegistry;
import io.github.quickmsg.common.utils.MessageUtils;
import io.netty.handler.codec.mqtt.MqttMessageType;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttPublishVariableHeader;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.util.context.ContextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author luxurong
 */
@Slf4j
public class PublishProtocol implements Protocol<MqttPublishMessage> {

    private static List<MqttMessageType> MESSAGE_TYPE_LIST = new ArrayList<>();

    static {
        MESSAGE_TYPE_LIST.add(MqttMessageType.PUBLISH);
    }

    @Override
    public List<MqttMessageType> getMqttMessageTypes() {
        return MESSAGE_TYPE_LIST;
    }


    @Override
    public Mono<Void> parseProtocol(SmqttMessage<MqttPublishMessage> smqttMessage, MqttChannel mqttChannel, ContextView contextView) {
        ReceiveContext<?> receiveContext = contextView.get(ReceiveContext.class);
        try {
            MetricManagerHolder.metricManager.getMetricRegistry().getMetricCounter(CounterType.PUBLISH_EVENT).increment();
            // 需要处理的消息
            MqttPublishMessage message = smqttMessage.getMessage();
            AclManager aclManager = receiveContext.getAclManager();
            if (!mqttChannel.getIsMock() && !aclManager.check(mqttChannel, message.variableHeader().topicName(), AclAction.PUBLISH)) {
                log.warn("mqtt【{}】publish topic 【{}】 acl not authorized ", mqttChannel.getConnection(), message.variableHeader().topicName());
                return Mono.empty();
            }
            // 主题
            TopicRegistry topicRegistry = receiveContext.getTopicRegistry();
            // 消息头
            MqttPublishVariableHeader variableHeader = message.variableHeader();
            // 消息注册地，定义在META-INF/service的地方
            MessageRegistry messageRegistry = receiveContext.getMessageRegistry();
            // 根据主题获取所有的订阅者Channel
            Set<SubscribeTopic> mqttChannels = topicRegistry.getSubscribesByTopic(variableHeader.topicName(),
                    message.fixedHeader().qosLevel());
            // http mock
            if (mqttChannel.getIsMock()) {
                // 如果是mock消息
                return send(mqttChannels, message, messageRegistry, filterRetainMessage(message, messageRegistry));
            }
            switch (message.fixedHeader().qosLevel()) {
                // 不同消息质量的处理方式
                case AT_MOST_ONCE:
                    // qos : 0 消息最多一次
                    return send(mqttChannels, message, messageRegistry, filterRetainMessage(message, messageRegistry));
                case AT_LEAST_ONCE:
                    // qos : 1 消息最少一次
                    return send(mqttChannels, message, messageRegistry,
                            // 写出消息成功后，进行应答
                            mqttChannel.write(
                                    MqttMessageBuilder.buildPublishAck(variableHeader.packetId()),
                                    false)
                                    .then(filterRetainMessage(message, messageRegistry)));
                case EXACTLY_ONCE:
                    // qos : 2 消息仅发送一次
                    if (!mqttChannel.existQos2Msg(variableHeader.packetId())) {
                        return mqttChannel
                                // 先缓存MQTT消息
                                .cacheQos2Msg(variableHeader.packetId(),
                                        MessageUtils.wrapPublishMessage(message, message.fixedHeader().qosLevel(), 0))
                                // 向当前管道写回接收到消息
                                .then(
                                        // 写回PUBREC后，当前Channel会再次写入PUBREL， 完成QOS=2最高质量的分发
                                        mqttChannel.write(
                                                // 封装为
                                                MqttMessageBuilder.buildPublishRec(variableHeader.packetId()),
                                                //
                                            true));
                    }
                default:
                    return Mono.empty();
            }
        } catch (Exception e) {
            log.error("error ", e);
        }
        return Mono.empty();
    }


    /**
     * 通用发送消息
     *
     * @param subscribeTopics {@link SubscribeTopic}
     * @param message         {@link MqttPublishMessage}
     * @param messageRegistry {@link MessageRegistry}
     * @param other           {@link Mono}
     * @return Mono
     */
    private Mono<Void> send(Set<SubscribeTopic> subscribeTopics, MqttPublishMessage message, MessageRegistry messageRegistry, Mono<Void> other) {
        // Mono.when, 等待所有的Mono执行完成
        return Mono.when(
                subscribeTopics.stream()
                        // 只需要给在线的设备发送消息
                        .filter(subscribeTopic -> filterOfflineSession(subscribeTopic.getMqttChannel(), messageRegistry, message))
                        // 消息转换
                        .map(subscribeTopic ->
                                // 给每一个Channel发送消息
                                subscribeTopic.getMqttChannel().write(
                                        // 封装需要发送给订阅者的消息
                                        MessageUtils.wrapPublishMessage(
                                                message,
                                                subscribeTopic.getQoS(),
                                                subscribeTopic.getMqttChannel().generateMessageId()),
                                        // 如果qos=1，需要进行重试
                                        subscribeTopic.getQoS().value() > 0)
                        )
                        .collect(Collectors.toList())).then(other);

    }


    /**
     * 过滤离线会话消息
     *
     * @param mqttChannel     {@link MqttChannel}
     * @param messageRegistry {@link MessageRegistry}
     * @param mqttMessage     {@link MqttPublishMessage}
     * @return boolean
     */
    private boolean filterOfflineSession(MqttChannel mqttChannel, MessageRegistry messageRegistry, MqttPublishMessage mqttMessage) {
        if (mqttChannel.getStatus() == ChannelStatus.ONLINE) {
            // 在线则返回true
            return true;
        } else {
            // 离线则保存MQTT消息
            messageRegistry
                    .saveSessionMessage(SessionMessage.of(mqttChannel.getClientIdentifier(), mqttMessage));
            return false;
        }
    }


    /**
     * 过滤保留消息
     *
     * @param message         {@link MqttPublishMessage}
     * @param messageRegistry {@link MessageRegistry}
     * @return Mono
     */
    private Mono<Void> filterRetainMessage(MqttPublishMessage message, MessageRegistry messageRegistry) {
        return Mono.fromRunnable(() -> {
            // 如果MQTT消息的固定头中，表明了需要保留消息，当前MQTT消息将存储到DB或者Redis缓存中
            if (message.fixedHeader().isRetain()) {
                // 保存
                messageRegistry.saveRetainMessage(RetainMessage.of(message));
            }
        });
    }


}
