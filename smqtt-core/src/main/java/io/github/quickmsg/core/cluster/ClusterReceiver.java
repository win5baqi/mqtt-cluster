package io.github.quickmsg.core.cluster;

import io.github.quickmsg.common.channel.MockMqttChannel;
import io.github.quickmsg.common.config.BootstrapConfig;
import io.github.quickmsg.common.message.HeapMqttMessage;
import io.github.quickmsg.common.cluster.ClusterRegistry;
import io.github.quickmsg.common.message.MqttMessageBuilder;
import io.github.quickmsg.common.message.SmqttMessage;
import io.github.quickmsg.common.protocol.ProtocolAdaptor;
import io.github.quickmsg.core.mqtt.MqttReceiveContext;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.handler.codec.mqtt.MqttMessage;
import io.netty.handler.codec.mqtt.MqttQoS;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

/**
 * @author luxurong
 */
@Slf4j
public class ClusterReceiver {

    private final MqttReceiveContext mqttReceiveContext;


    public ClusterReceiver(MqttReceiveContext mqttReceiveContext) {
        this.mqttReceiveContext = mqttReceiveContext;
    }

    public void registry() {
        // 获取集群配置
        BootstrapConfig.ClusterConfig clusterConfig = mqttReceiveContext.getConfiguration().getClusterConfig();
        // 获取集群的注册处
        ClusterRegistry clusterRegistry = mqttReceiveContext.getClusterRegistry();
        // 获取协议适配器
        ProtocolAdaptor protocolAdaptor = mqttReceiveContext.getProtocolAdaptor();
        // 是否开启集群
        if (clusterConfig.isEnable()) {
            // 基于JVM内存建立集群注册处
            if (clusterRegistry instanceof InJvmClusterRegistry) {
                // 每隔两秒打印日志
                Flux.interval(Duration.ofSeconds(2))
                        .subscribe(index -> log.warn("please set  smqtt-registry dependency  "));
            }
            // 基于其他建立集群注册处
            else {
                //registry cluster
                clusterRegistry.registry(clusterConfig);
                //begin listen cluster message
                // 从集群消息中心，获取消息进行处理
                clusterRegistry.handlerClusterMessage()
                        .doOnError(throwable -> log.error("cluster accept",throwable))
                        .onErrorResume(e-> Mono.empty())
                        .subscribe(clusterMessage -> protocolAdaptor
                                .chooseProtocol(MockMqttChannel.wrapClientIdentifier(clusterMessage.getClientIdentifier()),
                                        getMqttMessage(clusterMessage),
                                        mqttReceiveContext));
            }
        }
    }

    private SmqttMessage<MqttMessage> getMqttMessage(HeapMqttMessage heapMqttMessage) {
        return new SmqttMessage<>(MqttMessageBuilder
                .buildPub(false,
                        MqttQoS.valueOf(heapMqttMessage.getQos()),
                        0,
                        heapMqttMessage.getTopic(),
                        PooledByteBufAllocator.DEFAULT.buffer().writeBytes(heapMqttMessage.getMessage()),
                        heapMqttMessage.getProperties()), System.currentTimeMillis(), Boolean.TRUE);
    }

}
