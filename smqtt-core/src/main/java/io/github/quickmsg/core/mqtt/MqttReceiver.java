package io.github.quickmsg.core.mqtt;

import io.github.quickmsg.common.Receiver;
import io.github.quickmsg.common.channel.MqttChannel;
import io.github.quickmsg.core.ssl.AbstractSslHandler;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelOption;
import io.netty.channel.WriteBufferWaterMark;
import io.netty.handler.codec.mqtt.MqttDecoder;
import io.netty.handler.codec.mqtt.MqttEncoder;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.tcp.TcpServer;
import reactor.util.context.ContextView;

/**
 * @author luxurong
 */
public class MqttReceiver extends AbstractSslHandler implements Receiver {

    @Override
    public Mono<DisposableServer> bind() {
        return Mono.deferContextual(contextView -> Mono.just(this.newTcpServer(contextView)))
                // flatMap ： 对上一步产生的TcpServer，获取到并进一步处理
                .flatMap(view -> view.bind().cast(DisposableServer.class));
    }

    private TcpServer newTcpServer(ContextView context) {
        // 从上下文获取MQTT接收
        MqttReceiveContext receiveContext = context.get(MqttReceiveContext.class);
        // 获取
        MqttConfiguration mqttConfiguration = receiveContext.getConfiguration();
        // 初始化TcpServer
        TcpServer server = initTcpServer(mqttConfiguration);
        return server.port(mqttConfiguration.getPort())
                .wiretap(mqttConfiguration.getWiretap())
                .childOption(ChannelOption.WRITE_BUFFER_WATER_MARK, new WriteBufferWaterMark(mqttConfiguration.getLowWaterMark(), mqttConfiguration.getHighWaterMark()))
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.SO_REUSEADDR, true)
                .metrics(mqttConfiguration.getMeterConfig() != null)
                .option(ChannelOption.SO_REUSEADDR, true)
                .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .runOn(receiveContext.getLoopResources())
                // 创建连接时初始化
                .doOnConnection(connection -> {
                    connection
                             // 使用MQTT进行编码为字节流
                            .addHandler(MqttEncoder.INSTANCE)
                             // 使用MQTT解码
                            .addHandler(new MqttDecoder(mqttConfiguration.getMessageMaxSize()))
                             // 控制读写流量
                            .addHandler(receiveContext.getTrafficHandlerLoader().get());
                    //
                    receiveContext.apply(MqttChannel.init(connection,receiveContext.getTimeAckManager()));
                });
    }
}