package io.github.quickmsg.common.config;

/**
 * @author luxurong
 */
public enum ConnectModel {
    // 连接模型 - 不允许继续相同身份ID的连接
    UNIQUE,
    // 连接模型，踢掉相同身份ID
    KICK
}
